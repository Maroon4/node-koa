
const {Server} = require('http');

const server = new Server();

server.on('request', (req, res) => {
    res.end('hello');
});

server.listen(8000);